package authorizenet

import (
	"bytes"
	"crypto/hmac"
	"crypto/md5"
	"encoding/hex"
	"io"
	"strconv"
)

func GetFingerprint(api_login_id string, transaction_key string, amount float64, fp_sequence int64, timestamp int64) string {
	hkey := bytes.NewBufferString(transaction_key)
	hasher := hmac.New(md5.New, hkey.Bytes())
	io.WriteString(hasher, api_login_id+"^"+strconv.FormatInt(fp_sequence, 10)+"^"+strconv.FormatInt(timestamp, 10)+"^"+strconv.FormatFloat(amount, 'f', 2, 64)+"^")
	result := hex.EncodeToString(hasher.Sum(nil))
	// fmt.Println(result)
	return result
}
