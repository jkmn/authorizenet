package authorizenet

import (
	"bytes"
	"encoding/json"
	"log"
	"os"
)

type Settings struct {
	LoginID        string
	TransactionKey string
	GatewayID      string
	MD5Setting     string
	TestMode       bool
}

var settings Settings

func init() {
	var (
		file     *os.File
		err      error
		errFile  error
		filename = "authorizenet.json"
	)

	if file, errFile = os.Open(filename); errFile != nil {
		if os.IsNotExist(errFile) {
			log.Println("The file, " + filename + ", does not exist. A new one is being created for you.")

			if file, err = os.Create(filename); err != nil {
				log.Println("Failure creating the new " + filename + ": " + err.Error())
				file.Close()
			}

			var sampleSettings Settings

			if dataJSON, errJSON := json.MarshalIndent(sampleSettings, "", "  "); errJSON != nil {
				log.Println("Failure to marshall settings into JSON: " + errJSON.Error())
			} else {
				bufJSON := bytes.NewBuffer(dataJSON)
				if _, err = bufJSON.WriteTo(file); err != nil {
					log.Println("Failure to write settings into new " + filename + ": " + err.Error())
				}
			}
		}
		log.Fatalln(errFile)
	}

	defer file.Close()

	// Read the file, unmarshall it into the package settings variable.

	buf := new(bytes.Buffer)
	if _, err = buf.ReadFrom(file); err != nil {
		log.Fatalln("Failure reading " + filename + ": " + err.Error())
	}

	if err = json.Unmarshal(buf.Bytes(), &settings); err != nil {
		log.Fatalln("Failure decoding " + filename + ": " + err.Error())
	}
}
