authorizenet
============

Go library implementing Authorize.Net's Advanced Integration Method.

This library is based on __Advanced Integration Method (AIM) Developer Guide__ provided by the Authorize.Net developer site.

Authorize.Net's description of AIM
----------------------------------

AIM is a customizable payment processing solution that gives the merchant control over all of the steps in processing a transaction, including:

* Collecting customer payment information through a custom application
* Generating a receipt to the customer
* Secure transmission of data to the payment gateway for transaction processing
* Securely storing cardholder information
* And more, depending on the merchant’s business requirements

The security of an AIM transaction is ensured through a 128-bit Secure Sockets Layer (SSL) connection between the merchant’s Web server and the Authorize.Net Payment Gateway.

AIM allows merchants the highest degree of customization and control over their customer checkout experience.

Project Status
--------------

This library successfully processes AUTH_CAPTURE requests using the Authorize.Net test server. I am confident this code is reasonably secure and is ready for peer review. Due to the secure nature of the data this code is supposed to process, I'm not going to say 'ready for production' until I get a couple of other people to sign off on it. I will continue to work on this library as needed. Please feel free to fork, drop a note, or do whatever it is you want to do to participate.

Discussion of Efficiency and Ease of Maintenance
------------------------------------------------

In the comments of the code, you'll see my thoughts about using reflect versus explicit declarations. I keep getting told that reflection is very costly. My argument for using reflection is that maintainability reusability is more important than performance at this time. On my to-do list is a performance analysis of the code to see how it actually does. I anticipate that the HTTP request to the Authorize.Net server will always be the limiting part of execution, but I'd love to hear someone's argument either way.
