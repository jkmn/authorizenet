package authorizenet

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/jackmanlabs/address"
	"io"
	"net/http"
	"net/url"
	"reflect"
	"strconv"
	"strings"
)

/*

Authorization and Capture (AUTH_CAPTURE)

This is the most common type of credit card transaction and is the default
payment gateway transaction type. The amount is sent for authorization, and if
approved, is automatically submitted for settlement.

 x_type=AUTH_CAPTURE

Authorization Only (AUTH_ONLY)

This transaction type is sent for authorization only. The transaction will not
be sent for settlement until the credit card transaction type Prior
Authorization and Capture (see definition below) is submitted, or the
transaction is submitted for capture manually in the Merchant Interface. For
more information about capturing Authorization Only transactions in the Merchant
Interface, see the Merchant Integration Guide at
http://www.authorize.net/support/merchant/.

 x_type=AUTH_ONLY

Prior Authorization and Capture (PRIOR_AUTH_CAPTURE)

This transaction type is used to complete an Authorization Only transaction that
was successfully authorized through the payment gateway.

 x_type=PRIOR_AUTH_CAPTURE
 x_trans_id=TransactionID

Capture Only (CAPTURE_ONLY)

This transaction type is used to complete a previously authorized transaction
that was not originally submitted through the payment gateway or that requires
voice authorization.

 x_type=CAPTURE_ONLY
 x_auth_code=AuthorizationCode

Credit/Refund (CREDIT)

This transaction type is used to refund a customer for a transaction that was
originally processed and successfully settled through the payment gateway.

 x_type=CREDIT
 x_trans_id=TransactionID
 x_card_num=CardNumber

Void (VOID)

This transaction type can be used to cancel either an original transaction that
is not yet settled, or an entire order composed of more than one transactions. A
Void prevents the transaction or the order from being sent for settlement. A
Void can be submitted against any other transaction type.

 x_type=VOID
 x_trans_id=TransactionID OR x_split_tender_id=SplitTenderID
*/
type Request struct {
	/*
		Sensitive fields are not exported to prevent accidental storage in a DB.
		Fields that should not be exported on the basis of them being
		unimportant outside the package are exported so they can be stored in a
		DB and kept for	debugging.
	*/

	Amount            float64 `post:"x_amount"`          // Up to 15 digits.
	ExpirationDate    string  `post:"x_exp_date"`        // MMYYYY
	cardNumber        string  `post:"x_card_num"`        // 13-16 digits, last 4 if CREDIT
	login             string  `post:"x_login"`           // Merchant API Login ID
	transactionKey    string  `post:"x_tran_key"`        // Merchant Transaction Key
	transactionType   string  `post:"x_type"`            // AUTH_CAPTURE (default), AUTH_ONLY, CAPTURE_ONLY, CREDIT, PRIOR_AUTH_CAPTURE, VOID
	transactionID     string  `post:"x_trans_id"`        // Required for CREDIT, PRIOR_AUTH_CAPTURE, VOID
	splitTenderID     string  `post:"x_split_tender_id"` // Numeric; issued by gateway.
	authCode          string  `post:"x_auth_code"`       // 6 chars; required for CAPTURE_ONLY
	relayResponse     bool    `post:"x_relay_response"`  // Always false for AIM.
	delimitedResponse bool    `post:"x_delim_data"`      // Always true for AIM.

	// Everything below here is, for the most part, optional.

	CurrencyCode           string  `post:"x_currency_code"`      //
	Description            string  `post:"x_description"`        //
	NameFirst              string  `post:"x_first_name"`         // 14:
	NameLast               string  `post:"x_last_name"`          // 15:
	Company                string  `post:"x_company"`            // 16:
	Address                string  `post:"x_address"`            // 17:
	City                   string  `post:"x_city"`               // 18:
	State                  string  `post:"x_state"`              // 19:
	ZIPCode                string  `post:"x_zip"`                // 20:
	Country                string  `post:"x_country"`            // 21:
	Phone                  string  `post:"x_phone"`              // 22:
	Fax                    string  `post:"x_fax"`                // 23:
	Email                  string  `post:"x_email"`              // 24:
	ShipToNameFirst        string  `post:"x_ship_to_first_name"` // 25:
	ShipToNameLast         string  `post:"x_ship_to_last_name"`  // 26:
	ShipToCompany          string  `post:"x_ship_to_company"`    // 27:
	ShipToAddress          string  `post:"x_ship_to_address"`    // 28:
	ShipToCity             string  `post:"x_ship_to_city"`       // 29:
	ShipToState            string  `post:"x_ship_to_state"`      // 30:
	ShipToZIPCode          string  `post:"x_ship_to_zip"`        // 31:
	ShipToCountry          string  `post:"x_ship_to_country"`    // 32:
	CustomerID             string  `post:"x_cust_id"`            // 13: The merchant-assigned customer ID
	CustomerIP             string  `post:"x_customer_ip"`        // 13: IPv4 address of the customer.
	Tax                    float64 `post:"x_tax"`                // 33: Tax amount charged.
	Duty                   float64 `post:"x_duty"`               // 34: Duty amount charged.
	Freight                float64 `post:"x_freight"`            // 35: Freight amount charged.
	TaxExempt              bool    `post:"x_tax_exempt"`         // 36:
	cardCode               string  `post:"x_card_code"`          //
	delimitingCharacter    string  `post:"x_delim_char"`         //
	encapsulatingCharacter string  `post:"x_encap_char"`         //
	ApiVersion             string  `post:"x_version"`            //
	TestRequest            bool    `post:"x_test_request"`       //

	// Use of strings where normally runes would be used is due to me not knowing
	// how to properly handle them with the reflect package. --JACKMAN
}

func NewRequest() *Request {
	request := new(Request)

	request.login = settings.LoginID
	request.transactionKey = settings.TransactionKey
	request.relayResponse = false
	request.delimitedResponse = true
	request.delimitingCharacter = "|"
	request.encapsulatingCharacter = ""
	request.CurrencyCode = "USD"
	request.ApiVersion = "3.1"
	request.TestRequest = settings.TestMode

	return request
}

/*
This method is lossy. AIM requests do not store all of the address information
that is provided by address.Address type.
*/
func (request *Request) SetAddressMailing(addr *address.Address) {
	request.ShipToNameFirst = addr.NameFirst
	request.ShipToNameLast = addr.NameLast
	request.ShipToCompany = addr.Company
	request.ShipToAddress = addr.Street1
	request.ShipToCity = addr.City
	request.ShipToState = addr.StateProvince
	request.ShipToZIPCode = addr.PostalCode
	request.ShipToCountry = addr.Country
}

/*
This method is lossy. AIM requests do not store all of the address information
that is provided by address.Address type.
*/
func (request *Request) SetAddressBilling(addr *address.Address) {
	request.NameFirst = addr.NameFirst
	request.NameLast = addr.NameLast
	request.Company = addr.Company
	request.Address = addr.Street1
	request.City = addr.City
	request.State = addr.StateProvince
	request.ZIPCode = addr.PostalCode
	request.Country = addr.Country
	request.Phone = addr.Phone
	request.Fax = addr.Fax
	request.Email = addr.Email
}

// This setter is here because of the sensitive nature of the data.
func (request *Request) SetCard(card, exp, code string) {
	request.cardNumber = card
	request.ExpirationDate = exp
	request.cardCode = code
}

func (request *Request) Submit() (*Response, error) {
	var (
		response *Response      // Not using a pointer for the sake of reflection.
		dataOut  url.Values     = make(url.Values)
		respHTTP *http.Response // This is confusing, I know. I lack imagination.
		err      error
		url      string
	)

	// If anyone wants to have a discussion about the pros and cons of using
	// an explicit assignment instead of reflection, I'd be interested.
	// jackman@moonman.biz

	requestType := reflect.TypeOf(*request)
	requestValue := reflect.ValueOf(*request)
	for i := 0; i < requestType.NumField(); i++ {
		var field reflect.StructField = requestType.Field(i)
		postKey := field.Tag.Get("post")
		fieldKind := requestValue.Field(i).Kind()
		fieldValue := requestValue.Field(i)

		switch fieldKind {
		case reflect.Bool:
			dataOut[postKey] = []string{fmt.Sprintf("%t", fieldValue.Bool())}
		case reflect.String:
			dataOut[postKey] = []string{fieldValue.String()}
		case reflect.Float64:
			dataOut[postKey] = []string{fmt.Sprintf("%.2f", fieldValue.Float())}
		}
	}

	if settings.TestMode {
		url = urlTest
	} else {
		url = urlLive
	}

	if respHTTP, err = http.PostForm(url, dataOut); err != nil {
		return response, errors.New("Failure posting to Authorize.Net test server: " + err.Error())
	}

	outputBuffer := bytes.NewBuffer(nil)

	io.Copy(outputBuffer, respHTTP.Body)
	defer respHTTP.Body.Close()

	// I contemplated using a scanner ("text/scanner") here, but I decided that
	// the few number of fields (unlikely to exceed 100), would not need it.
	// -- JACKMAN

	response = new(Response)
	responseValue := reflect.ValueOf(response).Elem()
	fieldStrings := strings.Split(outputBuffer.String(), "|")
	for i, fieldString := range fieldStrings {

		fieldValue := responseValue.Field(i)

		// If the field is not exportable (probably reserved), skip it.
		if !fieldValue.CanSet() {
			continue
		}

		fieldKind := responseValue.Field(i).Kind()
		switch fieldKind {

		case reflect.Bool:
			if value, err := strconv.ParseBool(fieldString); err != nil {
				return response, errors.New("Failng parsing boolean (" + fieldString + "):\n" + err.Error())
			} else {
				fieldValue.SetBool(value)
			}

		case reflect.Int:
			if value, err := strconv.ParseInt(fieldString, 10, 64); err != nil {
				return response, errors.New("Failng parsing int (" + fieldString + "):\n" + err.Error())
			} else {
				fieldValue.SetInt(value)
			}

		case reflect.Int64:
			if value, err := strconv.ParseInt(fieldString, 10, 64); err != nil {
				return response, errors.New("Failng parsing int64 (" + fieldString + "):\n" + err.Error())
			} else {
				fieldValue.SetInt(value)
			}

		case reflect.Float64:
			// Do not try to parse empty strings.
			if fieldString == "" {
				continue
			}

			if value, err := strconv.ParseFloat(fieldString, 64); err != nil {
				return response, errors.New("Failng parsing float64 (" + fieldString + "):\n" + err.Error())
			} else {
				fieldValue.SetFloat(value)
			}

		case reflect.String:
			fieldValue.SetString(fieldString)

		}
	}

	return response, nil
}
