package authorizenet

import (
	"crypto/md5"
	"fmt"
	"github.com/jackmanlabs/address"
)

/*
The order of the members of this struct are VERY important. Authorize.Net sends
its response without any additional structure than the order. BEWARE!

This layout is specifically for Version 3.1 of AIM.

I'm including the POST values here because they are useful for the direct post
method. They were conveniently sourced form my earlier work. -- JACKMAN
*/
type Response struct {
	ResponseCode       int     `post:"x_response_code"`            // 01: 1(Approved), 2 (Declined), 3 (Error), 4 (Held for Review)
	ResponseSubCode    int     `post:"x_response_subcode"`         // 02:
	ResponseReasonCode int     `post:"x_response_reason_code"`     // 03: Numeric
	ResponseReasonText string  `post:"x_response_reason_text"`     // 04: Text; a brief description or result.
	AuthorizationCode  string  `post:"x_auth_code"`                // 05: Six characters.
	AVSResponse        string  `post:"x_avs_code"`                 // 06: Various characters, The Address Verification Service (AVS) response code.
	TransactionID      int64   `post:"x_trans_id"`                 // 07: The payment gateway-assigned identification number for the transaction.
	InvoiceNumber      string  `post:"x_invoice_num"`              // 08: The merchant-assigned invoice number for the transaction.
	Description        string  `post:"x_description"`              // 09: Transaction description.
	Amount             float64 `post:"x_amount"`                   // 10: Transaction amount.
	Method             string  `post:"x_method"`                   // 11: Payment method. {CC, ECHECK}
	TransactionType    string  `post:"x_type"`                     // 12: Type of CC transaction. {AUTH_CAPTURE, AUTH_ONLY, CAPTURE_ONLY, CREDIT, PRIOR_AUTH_CAPTUREVOID}
	CustomerID         string  `post:"x_cust_id"`                  // 13: The merchant-assigned customer ID
	NameFirst          string  `post:"x_first_name"`               // 14:
	NameLast           string  `post:"x_last_name"`                // 15:
	Company            string  `post:"x_company"`                  // 16:
	Address            string  `post:"x_address"`                  // 17:
	City               string  `post:"x_city"`                     // 18:
	State              string  `post:"x_state"`                    // 19:
	ZIPCode            string  `post:"x_zip"`                      // 20:
	Country            string  `post:"x_country"`                  // 21:
	Phone              string  `post:"x_phone"`                    // 22:
	Fax                string  `post:"x_fax"`                      // 23:
	Email              string  `post:"x_email"`                    // 24:
	ShipToNameFirst    string  `post:"x_ship_to_first_name"`       // 25:
	ShipToNameLast     string  `post:"x_ship_to_last_name"`        // 26:
	ShipToCompany      string  `post:"x_ship_to_company"`          // 27:
	ShipToAddress      string  `post:"x_ship_to_address"`          // 28:
	ShipToCity         string  `post:"x_ship_to_city"`             // 29:
	ShipToState        string  `post:"x_ship_to_state"`            // 30:
	ShipToZIPCode      string  `post:"x_ship_to_zip"`              // 31:
	ShipToCountry      string  `post:"x_ship_to_country"`          // 32:
	Tax                float64 `post:"x_tax"`                      // 33: Tax amount charged.
	Duty               float64 `post:"x_duty"`                     // 34: Duty amount charged.
	Freight            float64 `post:"x_freight"`                  // 35: Freight amount charged.
	TaxExempt          bool    `post:"x_tax_exempt"`               // 36:
	PONumber           string  `post:"x_po_num"`                   // 37: Merchant-provided PO Number.
	MD5Hash            string  `post:"x_MD5_Hash"`                 // 38: The payment gateway-generated MD5 hash value that can be used to authenticate the transaction response.
	CardCodeResponse   string  `post:"x_cvv2_resp_code"`           // 39: The card code verification (CCV) response code. {M,N,P,S,U}
	CAVVResponse       string  `post:"x_cavv_response"`            // 40: The cardholder authentication verification response code. {blank,hex}
	reserved41         bool    `post:"-"`                          // 41: Reserved
	reserved42         bool    `post:"-"`                          // 42: Reserved
	reserved43         bool    `post:"-"`                          // 43: Reserved
	reserved44         bool    `post:"-"`                          // 44: Reserved
	reserved45         bool    `post:"-"`                          // 45: Reserved
	reserved46         bool    `post:"-"`                          // 46: Reserved
	reserved47         bool    `post:"-"`                          // 47: Reserved
	reserved48         bool    `post:"-"`                          // 48: Reserved
	reserved49         bool    `post:"-"`                          // 49: Reserved
	reserved50         bool    `post:"-"`                          // 50: Reserved
	AccountNumber      string  `post:"x_account_number"`           // 51: Last 4 digits of the card provided
	CardType           string  `post:"x_card_type"`                // 52: {Visa, MasterCard, American Express, Discover, Diners Club, JCB}
	SplitTenderID      string  `post:"x_split_tender_id"`          // 53: This is only returned in the reply message for the first transaction that receives a partial authorization.
	RequestedAmount    float64 `post:"x_prepaid_requested_amount"` // 54: Amount requested in the original authorization, for split tenders.
	BalanceOnCard      float64 `post:"x_prepaid_balance_on_card"`  // 55: For debit or prepaid cards.
	reserved56         bool    `post:"-"`                          // 56: Reserved
	reserved57         bool    `post:"-"`                          // 57: Reserved
	reserved58         bool    `post:"-"`                          // 58: Reserved
	reserved59         bool    `post:"-"`                          // 59: Reserved
	reserved60         bool    `post:"-"`                          // 60: Reserved
	reserved61         bool    `post:"-"`                          // 61: Reserved
	reserved62         bool    `post:"-"`                          // 62: Reserved
	reserved63         bool    `post:"-"`                          // 63: Reserved
	reserved64         bool    `post:"-"`                          // 64: Reserved
	reserved65         bool    `post:"-"`                          // 65: Reserved
	reserved66         bool    `post:"-"`                          // 66: Reserved
	reserved67         bool    `post:"-"`                          // 67: Reserved
	reserved68         bool    `post:"-"`                          // 68: Reserved
	reserved69         bool    `post:"-"`                          // 69: Reserved

	// Everything after the 69th field is for merchant fields. This will be
	// implemented in the parser.
}

func (response *Response) GetAddressMailing() *address.Address {
	return &address.Address{
		NameFirst:     response.ShipToNameFirst,
		NameLast:      response.ShipToNameLast,
		Company:       response.ShipToCompany,
		Street1:       response.ShipToAddress,
		Street2:       "",
		Street3:       "",
		City:          response.ShipToCity,
		StateProvince: response.ShipToState,
		PostalCode:    response.ShipToZIPCode,
		Country:       response.ShipToCountry,
		Phone:         "",
		Fax:           "",
		Email:         "",
	}
}

func (response *Response) GetAddressBilling() *address.Address {
	return &address.Address{
		NameFirst:     response.NameFirst,
		NameLast:      response.NameLast,
		Company:       response.Company,
		Street1:       response.Address,
		Street2:       "",
		Street3:       "",
		City:          response.City,
		StateProvince: response.State,
		PostalCode:    response.ZIPCode,
		Country:       response.Country,
		Phone:         response.Phone,
		Fax:           response.Fax,
		Email:         response.Email,
	}
}

// This is not tested against Authorize.Net hashes.
func (response *Response) GenerateHash() string {
	preHash := fmt.Sprintf("%s%s%d%.2f", settings.MD5Setting, settings.LoginID, response.TransactionID, response.Amount)
	hash := md5.New()
	fmt.Fprintf(hash, preHash)
	postHash := fmt.Sprintf("%X", hash.Sum(nil))
	return postHash
}

func (r *Response) IsAuthorizeNet() bool {
	return (r.MD5Hash != "") && (r.GenerateHash() == r.MD5Hash)
}
